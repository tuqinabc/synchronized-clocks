import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DigitClockComponent } from './digit-clock/digit-clock.component';
import { AnalogClockComponent } from './analog-clock/analog-clock.component';
import { ClockSettingComponent } from './clock-setting/clock-setting.component';
import { MatRadioModule } from '@angular/material/radio';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    AppComponent,
    DigitClockComponent,
    AnalogClockComponent,
    ClockSettingComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    MatRadioModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
