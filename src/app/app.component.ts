import { Component, OnInit } from '@angular/core';
import { ClockService } from './service/clock.service';
import { Observable, combineLatest } from 'rxjs';
import { InterData } from './data/InterData';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'synchronized clocks';
  whichClock: number = 0;


  analogSecond: Observable<number>;
  analogMinute: Observable<number>;
  analogHour: Observable<number>;
  digitSecond: Observable<number>;
  digitMinute: Observable<number>;
  digitHour: Observable<number>;

  constructor(private clockService: ClockService) { }

  ngOnInit(): void {
    this.analogSecond = this.clockService.analogSecond;
    this.analogMinute = this.clockService.analogMinute;
    this.analogHour = this.clockService.analogHour;
    this.digitSecond = this.clockService.digitSecond;
    this.digitMinute = this.clockService.digitMinute;
    this.digitHour = this.clockService.digitHour;
  }
  /**
* onHourChanged
*/
  public onHourChanged(data: InterData) {
    const h = data.hour ? data.hour : 0;
    this.clockService.setWhichClock(data.whichClock);
    this.whichClock = data.whichClock;
    if (data.whichClock == 0) {
      this.clockService.setAnalogHour(h);
      this.clockService.setDigitHour(h);
    } else if (data.whichClock == 1) {
      this.clockService.setAnalogHour(h);
    } else if (data.whichClock = 2) {
      this.clockService.setDigitHour(h);
    }

  }

  /**
   * onMinuteChanged
   */
  public onMinuteChanged(data: InterData) {
    const m = data.minute ? data.minute : 0;
    this.clockService.setWhichClock(data.whichClock)
    this.whichClock = data.whichClock;
    if (data.whichClock == 0) {
      this.clockService.setAnalogMinute(m);
      this.clockService.setDigitMinute(m);
    } else if (data.whichClock == 1) {
      this.clockService.setAnalogMinute(m);
    } else if (data.whichClock = 2) {
      this.clockService.setDigitMinute(m);
    }
  }

  /**
   * onSecondChanged
   */
  public onSecondChanged(data: InterData) {
    const s = data.second ? data.second : 0;
    this.clockService.setWhichClock(data.whichClock)
    this.whichClock = data.whichClock;
    if (data.whichClock == 0) {
      this.clockService.setAnalogSecond(s);
      this.clockService.setDigitSecond(s);
    } else if (data.whichClock == 1) {
      this.clockService.setAnalogSecond(s);
    } else if (data.whichClock = 2) {
      this.clockService.setDigitSecond(s);
    }

  }

  onUpdateAll(data: InterData) {
    const h = data.hour ? data.hour : 0;
    const m = data.minute ? data.minute : 0;
    const s = data.second ? data.second : 0;
    this.whichClock = data.whichClock;
    this.clockService.setWhichClock(data.whichClock)
    this.clockService.setAnalogSecond(s);
    this.clockService.setDigitSecond(s);
    this.clockService.setAnalogMinute(m);
    this.clockService.setDigitMinute(m);
    this.clockService.setAnalogHour(h);
    this.clockService.setDigitHour(h);
  }

  choose(whichClock: number) {
    this.whichClock = whichClock;
  }


}
