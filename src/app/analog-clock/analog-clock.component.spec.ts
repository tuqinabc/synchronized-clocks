import { ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { ClockService } from '../service/clock.service';

import { AnalogClockComponent } from './analog-clock.component';

describe('AnalogClockComponent', () => {
  let component: AnalogClockComponent;
  let fixture: ComponentFixture<AnalogClockComponent>;
  let tbClockService: ClockService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AnalogClockComponent],
      providers: [ClockService]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AnalogClockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    tbClockService = TestBed.get(ClockService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('core should be injected ',
    inject([ClockService], (clockService: ClockService) => {
      expect(clockService).toBe(tbClockService);
    })
  );

});
