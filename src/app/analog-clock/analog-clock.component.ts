import { AfterViewChecked, Component, OnInit } from '@angular/core';
import { ClockService } from '../service/clock.service';
import { combineLatest, Observable } from 'rxjs';
import { ElementFinder } from 'protractor';

@Component({
  selector: 'app-analog-clock',
  templateUrl: './analog-clock.component.html',
  styleUrls: ['./analog-clock.component.scss']
})
export class AnalogClockComponent implements OnInit {

  private readonly offset = 180;
  public secondDegree: number = 0;
  public minuteDegree: number = 0;
  public hourDegree: number = 0;

  constructor(private clockService: ClockService) { }

  ngOnInit(): void {
    combineLatest(this.clockService.analogSecond,
      this.clockService.analogMinute,
      this.clockService.analogHour).subscribe(([second, minute, hour]) => this.updateTime(second, minute, hour));

  }

  updateTime(second: number, minute: number, hour: number) {
    this.secondDegree = this.offset + second * 6;
    this.minuteDegree = this.offset + (minute * 6) + (this.secondDegree / 60);
    this.hourDegree = this.offset + ((hour % 12) * 30) + (this.minuteDegree / 60);
  }

}
