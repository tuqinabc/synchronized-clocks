import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { InterData } from '../data/InterData';

@Component({
  selector: 'app-clock-setting',
  templateUrl: './clock-setting.component.html',
  styleUrls: ['./clock-setting.component.scss']
})
export class ClockSettingComponent implements OnInit {
  @Output() hourChanged = new EventEmitter<InterData>();
  @Output() secondChanged = new EventEmitter<InterData>();
  @Output() minuteChanged = new EventEmitter<InterData>();
  @Output() updateAllData = new EventEmitter<InterData>();
  @Output() flag = new EventEmitter<number>();

  @Input() hour: number = 0;
  @Input() minute: number = 0;
  @Input() second: number = 0;

  public whichClock: string = "0";
  constructor() { }

  ngOnInit(): void {
  }

  /**
 * onHourIncrease
 */
  public onHourIncrease() {
    this.hour = this.hour + 1;
    this.hourChanged.emit({ "whichClock": parseInt(this.whichClock), "hour": this.hour });
  }
  /**
   * onHourDecrease
   */
  public onHourDecrease() {
    this.hour = this.hour - 1;
    this.hourChanged.emit({ "whichClock": parseInt(this.whichClock), "hour": this.hour });
  }
  /**
   * onMinuteIncrease
   */
  public onMinuteIncrease() {
    this.minute = this.minute + 1;
    this.minuteChanged.emit({ "whichClock": parseInt(this.whichClock), "minute": this.minute });
  }

  /**
   * onMinuteDecrease
   */
  public onMinuteDecrease() {
    this.minute = this.minute - 1;
    this.minuteChanged.emit({ "whichClock": parseInt(this.whichClock), "minute": this.minute });
  }

  /**
   * onSecondIncrease
   */
  public onSecondIncrease() {
    this.second = this.second + 1;
    this.secondChanged.emit({ "whichClock": parseInt(this.whichClock), "second": this.second });
  }

  /**
   * onSecondDecrease
   */
  public onSecondDecrease() {
    this.second = this.second - 1;
    this.secondChanged.emit({ "whichClock": parseInt(this.whichClock), "second": this.second });
  }

  /**
   * updateAll
   */
  public updateAll() {
    this.updateAllData.emit({ "whichClock": parseInt(this.whichClock), "hour": this.hour, "minute": this.minute, "second": this.second });

  }

  public choose(whichClock: number) {
    this.flag.emit(whichClock);
  }

}
