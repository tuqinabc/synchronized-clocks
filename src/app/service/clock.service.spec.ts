import { TestBed } from '@angular/core/testing';

import { ClockService } from './clock.service';

describe('ClockService', () => {
  let service: ClockService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ClockService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should set the hour', () => {
    const service: ClockService = TestBed.get(ClockService);
    let hour = -1;
    service.digitHour.subscribe(value => hour = value);
    service.setDigitHour(10);
    expect(hour).toEqual(10);
  });
  it('should set the minute', () => {
    const service: ClockService = TestBed.get(ClockService);
    let minute = -1;
    service.digitMinute.subscribe(value => minute = value);
    service.setDigitMinute(10);
    expect(minute).toEqual(10);
  });
  it('should set the second', () => {
    const service: ClockService = TestBed.get(ClockService);
    let second = -1;
    service.digitSecond.subscribe(value => second = value);
    service.setDigitSecond(10);
    expect(second).toEqual(10);
  });
});
