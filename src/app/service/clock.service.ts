import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export abstract class ClockService {
  private analogSecondSubject = new BehaviorSubject(0);
  private analogMinuteSubject = new BehaviorSubject(0);
  private analogHourSubject = new BehaviorSubject(0);
  private whichClock: number = 0;
  private digitSecondSubject = new BehaviorSubject(0);
  private digitMinuteSubject = new BehaviorSubject(0);
  private digitHourSubject = new BehaviorSubject(0);

  constructor() {
    this.reset();

    setInterval(() => {
      const newAnalogSecond = (this.analogSecondSubject.value + 1) % 60;
      const newAnalogMinute = newAnalogSecond === 0 ? ((this.analogMinuteSubject.value + 1) % 60) : this.analogMinuteSubject.value;
      const newAnaglogHour = newAnalogMinute === 0 ? ((this.analogHourSubject.value + 1) % 24) : this.analogHourSubject.value;

      const newDigitSecond = (this.digitSecondSubject.value + 1) % 60;
      const newDigitMinute = newDigitSecond === 0 ? ((this.digitMinuteSubject.value + 1) % 60) : this.digitMinuteSubject.value;
      const newDigitHour = newDigitMinute === 0 ? ((this.digitHourSubject.value + 1) % 24) : this.digitHourSubject.value;

      const analogTime = [newAnaglogHour, newAnalogMinute, newAnalogSecond]
      const digitTime = [newDigitHour, newDigitMinute, newDigitSecond]
      this.setValue(analogTime, digitTime, this.whichClock);
    }, 1000);
  }


  reset() {
    const currentTime = new Date();
    // const time = ;
    this.setValue([currentTime.getHours(), currentTime.getMinutes(), currentTime.getSeconds()], [currentTime.getHours(), currentTime.getMinutes(), currentTime.getSeconds()]);
  }

  setValue(analogTime: number[], digitTime: number[], whichClock: number = 0) {
    if (whichClock == 0) {
      // analog = 1
      this.setAnalogHour(analogTime[0]);
      this.setAnalogMinute(analogTime[1]);
      this.setAnalogSecond(analogTime[2]);
      //digit = 2
      this.setDigitHour(digitTime[0]);
      this.setDigitMinute(digitTime[1]);
      this.setDigitSecond(digitTime[2]);
    } else if (whichClock == 1) {
      this.setAnalogHour(analogTime[0]);
      this.setAnalogMinute(analogTime[1]);
      this.setAnalogSecond(analogTime[2]);
    } else if (whichClock == 2) {
      this.setDigitHour(digitTime[0]);
      this.setDigitMinute(digitTime[1]);
      this.setDigitSecond(digitTime[2]);
    }
    this.whichClock = 0
  }

  public get analogSecond(): Observable<number> {
    return this.analogSecondSubject.asObservable();
  }
  public get analogMinute(): Observable<number> {
    return this.analogMinuteSubject.asObservable();
  }
  public get analogHour(): Observable<number> {
    return this.analogHourSubject.asObservable();
  }

  public get digitSecond(): Observable<number> {
    return this.digitSecondSubject.asObservable();
  }
  public get digitMinute(): Observable<number> {
    return this.digitMinuteSubject.asObservable();
  }
  public get digitHour(): Observable<number> {
    return this.digitHourSubject.asObservable();
  }




  setAnalogSecond(second: number) {
    this.analogSecondSubject.next((second < 0 ? second + 60 : second) % 60);
  }
  setAnalogMinute(minute: number) {
    this.analogMinuteSubject.next((minute < 0 ? minute + 60 : minute) % 60);
  }
  setAnalogHour(hour: number) {
    this.analogHourSubject.next((hour < 0 ? hour + 24 : hour) % 24);
  }

  setDigitSecond(second: number) {
    this.digitSecondSubject.next((second < 0 ? second + 60 : second) % 60);
  }
  setDigitMinute(minute: number) {
    this.digitMinuteSubject.next((minute < 0 ? minute + 60 : minute) % 60);
  }
  setDigitHour(hour: number) {
    this.digitHourSubject.next((hour < 0 ? hour + 24 : hour) % 24);
  }

  setWhichClock(whichClock: number) {
    this.whichClock = whichClock;
  }
}
