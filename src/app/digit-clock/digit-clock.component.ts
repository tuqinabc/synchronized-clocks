import { Component, OnInit } from '@angular/core';
import { ClockService } from '../service/clock.service';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-digit-clock',
  templateUrl: './digit-clock.component.html',
  styleUrls: ['./digit-clock.component.scss']
})
export class DigitClockComponent implements OnInit {
  second_: number = 0;
  minute_: number = 0;
  hour_: number = 0;
  constructor(private clockService: ClockService) { }

  ngOnInit(): void {
    combineLatest(this.clockService.digitSecond,
      this.clockService.digitMinute,
      this.clockService.digitHour).subscribe(([second, minute, hour]) => this.updateTime(second, minute, hour));
  }

  updateTime(s: number, m: number, h: number) {
    this.second_ = s;
    this.minute_ = m;
    this.hour_ = h;
  }




}
