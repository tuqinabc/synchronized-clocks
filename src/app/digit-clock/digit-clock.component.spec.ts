import { ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { ClockService } from '../service/clock.service';

import { DigitClockComponent } from './digit-clock.component';

describe('DigitClockComponent', () => {
  let component: DigitClockComponent;
  let fixture: ComponentFixture<DigitClockComponent>;
  let tbClockService: ClockService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DigitClockComponent],
      providers: [ClockService]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DigitClockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    tbClockService = TestBed.get(ClockService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('core should be injected ',
    inject([ClockService], (clockService: ClockService) => {
      expect(clockService).toBe(tbClockService);
    })
  );
});
