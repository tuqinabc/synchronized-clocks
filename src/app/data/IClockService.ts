import { Observable, BehaviorSubject } from 'rxjs';
export interface IClockService {
    second_: Observable<number>;
    minute_: Observable<number>;
    hour_: Observable<number>;

    reset(): void;
    setSecond(second: number): void;
    setMinute(minute: number): void;
    setHour(hour: number): void;
}