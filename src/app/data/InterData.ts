export interface InterData {
    "whichClock": number | 0,
    "hour"?: number | 0,
    "minute"?: number | 0,
    "second"?: number | 0
}